# Youtube Firewall Workaround

This program creates a simple window that accepts a youtube url and opens it in a way that gets around most firewalls. It uses Google Translate as a proxy to get around the firewall. So all this really does is open Google Translate and attempt to translate the youtube webpage from Spanish to English (Because that seems to keep all the text the same if it was originally in English).

How to Build With Pyinstaller
-----------

To appropriately get the icon referenced for the tkinter window in the .exe, you actually have to run Pyinstaller twice and edit the .spec file inbetween.

## Step 1

```
Pyinstaller -F --noconsole --icon ./Resources/Icon.ico ^
--name="Youtube Firewall Workaround" youtubefirewallworkaround.py
```

## Step 2
Change `datas=None,` to `datas=[ ('Resources/Icon.ico', 'Resources') ],` in the .spec file.

## Step 3

```
Pyinstaller "Youtube Firewall Workaround.spec"
```

