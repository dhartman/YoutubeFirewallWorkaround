#! python3

import os
import sys
import subprocess
from tkinter import Tk, Frame, Label, Entry, Button, Menu, TclError


def center(window):
    """ Center a tkinter window. """
    window.update_idletasks()
    w = window.winfo_screenwidth()
    h = window.winfo_screenheight()
    size = tuple(int(_) for _ in window.geometry().split('+')[0].split('x'))
    x = int((w - size[0])/2)
    # Y isn't exactly centered. int((h - size[1])/2) <-- would be centered
    y = int((h - size[1])/2.5)
    window.geometry('+{}+{}'.format(x, y))


def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller. """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)


def openUrl(youtubeUrl):
    """  Take a youtube url and reformat it in a way that translate.google.com can understand it. """
    youtubeUrl = youtubeUrl.replace(':', '%3A').replace('/', '%2F').replace('?', '%3F').replace('=', '%3D')
    link = '"https://translate.google.com/translate?hl=en&sl=es&tl=en&u=' + youtubeUrl + '"'
    subprocess.Popen('start "link" ' + link, shell=True)
    return


class YoutubeUrlMaker:
    """ Class for Gui """

    # default youtube url
    kyloRen = 'https://www.youtube.com/watch?v=FaOSCASqLsE'

    def __init__(self):
        self.root = Tk()
        self.root.attributes('-alpha', 0)
        self.root.title('Youtube Firewall Workaround')
        self.root.resizable(False, False)
        self.root.iconbitmap(resource_path('Resources\\Icon.ico'))

        self.mainFrame = Frame(self.root, relief='groove', border=2, padx=2.5, pady=2.5)

        self.urlLabel = Label(self.mainFrame, text='Youtube Url:')
        self.urlEntry = Entry(self.mainFrame, width=50, justify='right', foreground='lightgrey')
        self.urlEntry.insert(0, YoutubeUrlMaker.kyloRen)
        self.urlButton = Button(self.mainFrame, text='Open Url', command=self.buttonPress)

        # Menu for urlEntry activated by self.rightClick
        self.aMenu = Menu(self.root, tearoff=0)
        self.aMenu.add_command(label="Paste", command=self.pastePress)

        # Create actions for when urlEntry is clicked on (FocusIn) and clicked out of (FocusOut)
        self.urlEntry.bind("<FocusIn>", self.entryEvent)
        self.urlEntry.bind("<FocusOut>", self.entryEvent)
        # Bind right mouse button click to self.rightClick function
        self.urlEntry.bind("<Button-3>", self.rightClick)

        self.urlLabel.grid(row=0, column=0, sticky='w')
        self.urlEntry.grid(row=1, column=0)
        self.urlButton.grid(row=2, column=0)

        self.mainFrame.grid(padx=5, pady=5)

        # Add padding to all the rows in mainFrame
        self.mainFrame.grid_rowconfigure('all', pad=5)

        center(self.root)

        self.root.attributes('-alpha', 1)

        self.root.mainloop()

    def pastePress(self):
        """ Grabs (pastes) clipboard text into urlEntry """
        # If default url is still in urlEntry, delete it first and change the text color back to standard.
        try:
            clipboard = self.root.clipboard_get()
            if self.urlEntry.get() == YoutubeUrlMaker.kyloRen:
                self.urlEntry.delete(0, 'end')
                self.urlEntry.config(foreground='black')
            self.urlEntry.insert(0, self.root.clipboard_get())
        except TclError:
            pass

    def rightClick(self, event):
        """ Displays the right click menu at coordinates when the right click was clicked """
        self.aMenu.post(event.x_root, event.y_root)


    def entryEvent(self, event):
        """ Inserts default text when urlEntry is empty and deletes it when urlEntry is clicked. Also toggles text color
         if based on if the text is the default text or not. """
        current = self.urlEntry.get()
        if current == YoutubeUrlMaker.kyloRen:
            self.urlEntry.delete(0, 'end')
            self.urlEntry.config(foreground='black')
        elif current == "":
            self.urlEntry.insert(0, YoutubeUrlMaker.kyloRen)
            self.urlEntry.config(foreground='lightgrey')

    def buttonPress(self):
        """ urlButton click action. Grabs text from entry, runs it through openUrl function and closes window """
        youtubeUrl = self.urlEntry.get()
        openUrl(youtubeUrl)
        self.root.destroy()

if __name__ == '__main__':
    YoutubeUrlMaker()
